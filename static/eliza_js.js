var xhttp = new XMLHttpRequest();

function talk() {
	xhttp.open("POST", "http://130.245.168.142/eliza/DOCTOR/", true);
	xhttp.setRequestHeader("Content-type", "application/json");
	var humanInput = document.getElementById("human_input");
	xhttp.send(JSON.stringify({"human":humanInput.value}));
	xhttp.onreadystatechange = function() {
		if(xhttp.readyState == XMLHttpRequest.DONE && xhttp.status == 200){
			var response = JSON.parse(xhttp.responseText);
			var convo = document.getElementById("conversation");
			//Human message
			var node = document.createElement("div");
			var textNode = document.createTextNode("You: " + humanInput.value);
			node.appendChild(textNode);
			convo.appendChild(node);
			//Eliza message
			node = document.createElement("div");
			textNode = document.createTextNode("Eliza: " + response.eliza);
			node.appendChild(textNode);
			convo.appendChild(node);
		}
	}
}	
