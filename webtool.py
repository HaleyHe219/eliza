from flask import Flask, render_template, request, jsonify, send_from_directory
import time
import re
from random import randint
from pymongo import MongoClient
import smtplib

app = Flask(__name__, static_folder='static', static_url_path='/static')

@app.route('/', methods=['POST'])
def eliza():
	error = None
        name = request.form['name']
        if len(name) > 0:
            displayTime = time.asctime()
            return render_template("chat.html",
                                    name = name,
                                    time = displayTime)
        else:
	    error = "Please enter a name"

        return render_template('name.html',
		
		error = error)

@app.route('/DOCTOR/', methods=['POST'])
def chat():
	#REST CODE TO SENT TO JS SHOULD GO HERE
	#return jsonify({"eliza": "why do you feel that way?"})
	if not request.json or not 'human' in request.json:
		abort(400)
	q = request.json.get('human');
	a = "Please talk to me.";
	if len(q)==0:
		a = "Please talk to me."
	elif "you" in q:
		a = "Don't worry about me. Please talk about yourself."
	elif re.search(r'.?\bI feel\b.*', q, re.M|re.I):
		a =  "Why do you feel that way?"
	elif re.search(r'.?\bI felt\b.*', q, re.M|re.I):
		a = "Why did you feel that way?"
	elif re.search(r'.?\bI tell\b.*', q, re.M|re.I):
                a = "Tell me more about it."
	elif re.search(r'.?\bI think\b.*', q, re.M|re.I):
		a = "Why do you think so?"
	elif re.search(r'.?\bI thought\b.*', q, re.M|re.I):
                a = "Why did you think so?"
	elif "mother" in q or "sister" in q or "grandma" in q or "grandmother" in q or "aunt" in q:
                a = "What about her?"
	elif "father" in q or "brother" in q or "grandpa" in q or "grandfather" in q or "uncle" in q:
		a = "What about him?"
	elif "cousin" in q:
		a = "What about the cousin?"
	elif "nephew" in q:
		a = "What about the nephew?"
	else:
		others = ["Tell me more.", "Hmm... Go on.", "Interesting, please talk more about it."]
		pos = randint(0, len(others)-1)
		a = others[pos]
	return jsonify({"eliza": a})
		

@app.route('/register')
def register():
	#return send_from_directory(app.static_folder, request.path[1:])
	return render_template('register.html')

@app.route('/adduser', methods=['POST'])
def addUser():
	if not request.json:
		abort(404)
	username = request.json.get('username')
	password = request.json.get('password')
	email = request.json.get('email')
	client = MongoClient()
	db = client.wp2
	arbitraryKey = ObjectId()
	result = db.users.insert_one(
	{
		"username": username,
		"password": password,
		"email": email,
		"disabled": "true",
		"key": arbitraryKey
	})
	client.close()
	server = smtplib.SMTP('smtp.gmail.com', 587)
	server.starttls()
	server.login("130.245.168.142.eliza@gmail.com", "130.245.168.142")
	msg = "130.245.168.142/eliza/verify?email=" + email + "&key=" + arbitraryKey
	server.sendmail("130.245.168.142.eliza@gmail.com", email, msg)
	server.quit()
	return jsonify({"key": arbitraryKey})

@app.route('/verify', methods=['GET'])
def verify():
	#return render_template('verify.html')
        client = MongoClient()
        db = client.wp2
        email = request.args.get('email')
        key = request.args.get('key')
        try:
                document = db.users.update_one({"email": email, "key": key}, {"$set": {"disabled": "false"}})
        except Exception:
                return jsonify({"status": "ERROR"})
        return jsonify({"status": "OK"})

@app.route('/login')
def login(): 
        return render_template('login.html')

@app.route('/login', methods = ['POST'])
def processLogin():
	error = None
        user = request.form['user']
	password = request.form['pass']
	name = 'blah' #will need to change this line for cookies
        if len(user) > 0 and len(password) > 0:
            displayTime = time.asctime()
            return render_template("chat.html", name = name, time = displayTime)
        else:
            error = "Please enter a valid username and password combination."
	    return render_template('login.html', error = error) 
		

	
@app.route('/')
def index():
	return render_template('name.html')

if __name__ == "__main__":
    app.run()
